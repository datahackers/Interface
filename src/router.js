import Vue from 'vue'
import Router from 'vue-router'
import About from './views/About.vue'
import navbar from './views/Navbar.vue'
import card from './views/Card.vue'
import enscard from './views/ensArticle.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      components : {
        Navbar : navbar,
        Card : card,
        EnsCard : enscard
      }
    },
    {
      path: '/about',
      name: 'about',
      component: About
    }
  ]
})
